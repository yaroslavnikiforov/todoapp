import { types } from "../actions/types.js";

const loading = (state, action) => {
  switch (action.type) {
    case types.SET_LOADER:
      return true;
    case types.REMOVE_LOADER:
      return false;
    default:
      return false;
  }
};

export default loading;
