import { types } from "../actions/types.js";
import { pickBy } from "lodash";

export const getVisibleTodos = (state, filter) => {
  switch (filter) {
    case "SHOW_ALL":
      return state;
    case "SHOW_ACTIVE":
      return pickBy(state, todo => !todo.completed);
    case "SHOW_COMPLETED":
      return pickBy(state, todo => todo.completed);
  }
};

const todos = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_TODOS:
      return action.payload;
    default:
      return state;
  }
};

export default todos;
