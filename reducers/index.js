import { combineReducers } from "redux";
import todos, * as fromTodos from "./todos";
import visibilityFilter from "./visibilityFilter";
import loading from "./loading";

const rootReducer = combineReducers({
  todos,
  visibilityFilter,
  loading
});

export default rootReducer;

export const getVisibleTodos = state =>
  fromTodos.getVisibleTodos(state.todos, state.visibilityFilter);
