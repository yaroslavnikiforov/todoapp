import { types } from "./types.js";
import { database } from "../firebase";

export const getTodos = () => dispatch => {
  dispatch(setLoader());
  database.on("value", snap => {
    dispatch({
      type: types.FETCH_TODOS,
      payload: snap.val()
    });
    dispatch(removeLoader());
  });
};

export const saveTodo = todo => dispatch => {
  dispatch(setLoader());
  database.push(todo);
};

export const deleteTodo = key => dispatch => {
  dispatch(setLoader());
  database.child(key).remove();
};

export const toggleTodo = (key, completed) => dispatch => {
  dispatch(setLoader());
  completed
    ? database.child(key).update({
        completed: false
      })
    : database.child(key).update({
        completed: true
      });
};

export const setLoader = () => ({
  type: types.SET_LOADER
});

export const removeLoader = () => ({
  type: types.REMOVE_LOADER
});

export const setFilter = filter => ({
  type: types.SET_VISIBILITY_FILTER,
  filter
});
