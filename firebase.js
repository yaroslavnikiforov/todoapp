import * as firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyD-HFFdwaVnSUuc-SOWQuJmpypme--eUx8",
  databaseURL: "https://todoapp-2f883.firebaseio.com"
};

firebase.initializeApp(firebaseConfig);

export const database = firebase.database().ref("todos/");
