import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { saveTodo } from "../../actions";

class AddTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: ""
    };
  }

  _addTodo = () => {
    this.props.saveTodo({
      text: this.state.inputValue,
      completed: false
    });

    this.setState({
      inputValue: ""
    });
  };

  render() {
    return (
      <>
        <TextInput
          style={styles.input}
          placeholder="Type here new todo item"
          onChangeText={text => this.setState({ inputValue: text })}
          value={this.state.inputValue}
        />
        <TouchableOpacity style={styles.button} onPress={this._addTodo}>
          <View>
            <Text style={styles.text}>ADD TODO</Text>
          </View>
        </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    width: 200,
    height: 40,
    marginVertical: 20,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 10
  },
  button: {
    padding: 10,
    backgroundColor: "black",
    borderRadius: 10
  },
  text: {
    color: "white"
  }
});

export default connect(
  null,
  { saveTodo }
)(AddTodo);
