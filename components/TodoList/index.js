import React, { Component } from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import TodoItem from "./TodoItem";
import { getTodos } from "../../actions";
import { getVisibleTodos } from "../../reducers";
import { map } from "lodash";

class TodoList extends Component {
  componentWillMount() {
    this.props.getTodos();
  }

  render() {
    const { todos, loading } = this.props;

    return (
      <View style={styles.container}>
        {loading ? (
          <View style={styles.loader}>
            <ActivityIndicator size="large" color="#000000" />
          </View>
        ) : (
          map(todos, (todo, key) => (
            <TodoItem key={key} todoKey={key} todoItem={todo} />
          ))
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignSelf: "flex-start",
    paddingHorizontal: 20
  },
  loader: {
    marginTop: 50
  }
});

const mapStateToProps = state => ({
  todos: getVisibleTodos(state),
  loading: state.loading
});

export default connect(
  mapStateToProps,
  { getTodos }
)(TodoList);
