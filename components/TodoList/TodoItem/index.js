import React from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { toggleTodo, deleteTodo } from "../../../actions";

const TodoItem = ({ todoItem, todoKey, toggleTodo, deleteTodo }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={() => toggleTodo(todoKey, todoItem.completed)}>
      <View>
        <Text
          style={{ marginVertical: 5, opacity: todoItem.completed ? 0.5 : 1 }}
        >
          {"- " + todoItem.text}
        </Text>
      </View>
    </TouchableOpacity>
    <TouchableOpacity
      style={styles.closeButton}
      onPress={() => {
        deleteTodo(todoKey);
      }}
    >
      <View
        style={[styles.closeButtonLine, { transform: [{ rotateZ: "-45deg" }] }]}
      />
      <View
        style={[styles.closeButtonLine, { transform: [{ rotateZ: "45deg" }] }]}
      />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  closeButton: {
    position: "relative",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 15,
    height: 15,
    backgroundColor: "#a7a7a7",
    borderRadius: 50
  },
  closeButtonLine: {
    position: "absolute",
    width: 2,
    height: 10,
    backgroundColor: "#444444"
  }
});

export default connect(
  null,
  { toggleTodo, deleteTodo }
)(TodoItem);
