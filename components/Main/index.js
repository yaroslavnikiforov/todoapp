import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import AddTodo from "../AddTodo";
import TodoList from "../TodoList";
import VisibilityFilter from "../VisibilityFilter";

const Main = () => (
  <View style={styles.container}>
    <AddTodo />
    <VisibilityFilter />
    <TodoList />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 30,
    backgroundColor: "white"
  }
});

export default Main;
