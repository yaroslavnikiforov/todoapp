import React from "react";
import FilterLink from "./FilterLink";
import { StyleSheet, View } from "react-native";

const VisibilityFilter = () => (
  <View style={styles.container}>
    <FilterLink filter="SHOW_ALL">All</FilterLink>
    <FilterLink filter="SHOW_ACTIVE">Active</FilterLink>
    <FilterLink filter="SHOW_COMPLETED">Completed</FilterLink>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 200,
    marginVertical: 20
  }
});

export default VisibilityFilter;
