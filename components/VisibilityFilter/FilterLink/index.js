import React, { Component } from "react";
import { TouchableOpacity, Text, View } from "react-native";
import { connect } from "react-redux";
import { setFilter } from "../../../actions";

const FilterLInk = ({ children, activeFilter, setFilter }) =>
  activeFilter ? (
    <View>
      <Text style={{ color: "#c9c9c9" }}>{children}</Text>
    </View>
  ) : (
    <TouchableOpacity onPress={setFilter}>
      <View>
        <Text>{children}</Text>
      </View>
    </TouchableOpacity>
  );

const mapStateToProps = (state, ownProps) => ({
  activeFilter: ownProps.filter === state.visibilityFilter
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  setFilter() {
    dispatch(setFilter(ownProps.filter));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterLInk);
